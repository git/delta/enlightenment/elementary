# Spanish translation for elementary.
# Copyright (C) YEAR Enlightenment development team
# This file is distributed under the same license as the PACKAGE package.
# Aníbal Garrido <khany@member.trisquel.info>, 2012.
# Adrián Arévalo <adri58@gmail.com>, 2015.
# Roy W. Reese <waterbearer54@gmx.com>, 2015.
#: src/lib/elm_config.c:3271
msgid ""
msgstr ""
"Project-Id-Version: elementary\n"
"Report-Msgid-Bugs-To: enlightenment-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2015-11-09 11:01+0100\n"
"PO-Revision-Date: 2015-05-03 18:19+0100\n"
"Last-Translator: Adrián Arévalo <adri58@gmail.com>\n"
"Language-Team: Enlightenment Team\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.6\n"

#: src/lib/elc_fileselector.c:1505
msgid "Up"
msgstr "Arriba"

#: src/lib/elc_fileselector.c:1521
msgid "Home"
msgstr "Carpeta personal"

#: src/lib/elc_fileselector.c:1588
msgid "Search"
msgstr "Buscar"

#: src/lib/elc_fileselector.c:1757 src/lib/elm_entry.c:1588
#: src/lib/elm_entry.c:1613
msgid "Cancel"
msgstr "Cancelar"

#: src/lib/elc_fileselector.c:1767
msgid "OK"
msgstr "Aceptar"

#: src/lib/elc_multibuttonentry.c:670
msgid "multi button entry label"
msgstr "etiqueta de entrada multi-botón"

#: src/lib/elc_multibuttonentry.c:692
msgid "multi button entry item"
msgstr "elemento de entrada multi-botón"

#: src/lib/elc_multibuttonentry.c:1565
msgid "multi button entry"
msgstr "entrada multi-botón"

#: src/lib/elc_naviframe.c:432
msgid "Title"
msgstr "Título"

#: src/lib/elc_naviframe.c:978 src/lib/elc_naviframe.c:1183
msgid "Back"
msgstr "Atrás"

#: src/lib/elc_naviframe.c:1279
msgid "Next"
msgstr "Siguiente"

#: src/lib/elc_popup.c:275 src/lib/elc_popup.c:941
#, fuzzy
msgid "Popup Title"
msgstr "Nombre emergente"

#: src/lib/elc_popup.c:285 src/lib/elc_popup.c:1017
#, fuzzy
msgid "Popup Body Text"
msgstr "Texto emergente"

#: src/lib/elm_bubble.c:194
msgid "Bubble"
msgstr "Burbuja"

# Antiguemente: "Clickado"
#: src/lib/elm_button.c:66
msgid "Clicked"
msgstr "Seleccionado"

#: src/lib/elm_button.c:275 src/lib/elm_check.c:244 src/lib/elm_gengrid.c:1257
#: src/lib/elm_genlist.c:1659 src/lib/elm_list.c:2193 src/lib/elm_radio.c:268
#: src/lib/elm_segment_control.c:524 src/lib/elm_spinner.c:902
#: src/lib/elm_toolbar.c:2250
msgid "State: Disabled"
msgstr "Estado: Desactivado"

#: src/lib/elm_button.c:300
msgid "Button"
msgstr "Botón"

#: src/lib/elm_calendar.c:219
msgid "%B %Y"
msgstr "%B %Y"

#: src/lib/elm_calendar.c:225
msgid "%B"
msgstr "%B"

#: src/lib/elm_calendar.c:231
msgid "%Y"
msgstr "%Y"

#: src/lib/elm_calendar.c:345
msgid "calendar item"
msgstr "elemento de calendario"

#: src/lib/elm_calendar.c:371
msgid "calendar decrement month button"
msgstr "Botón de decremento del mes"

#: src/lib/elm_calendar.c:376
msgid "calendar decrement year button"
msgstr "Botón de decremento del año"

#: src/lib/elm_calendar.c:381
msgid "calendar increment month button"
msgstr "Botón de incremento del mes"

#: src/lib/elm_calendar.c:386
msgid "calendar increment year button"
msgstr "Botón de incremento del año"

#: src/lib/elm_calendar.c:391
msgid "calendar month"
msgstr "mes del calendario"

#: src/lib/elm_calendar.c:396
msgid "calendar year"
msgstr "año del calendario"

# RR: Traducción italiana usa ativado/desactivado. No sé a que refiere la cadena.
#: src/lib/elm_check.c:67 src/lib/elm_check.c:257 src/lib/elm_radio.c:125
#: src/lib/elm_radio.c:269
msgid "State: On"
msgstr "Estado: Conectado"

#: src/lib/elm_check.c:78 src/lib/elm_check.c:269 src/lib/elm_radio.c:271
msgid "State: Off"
msgstr "Estado: Desconectado"

#: src/lib/elm_check.c:253 src/lib/elm_check.c:266
msgid "State"
msgstr "Estado"

#: src/lib/elm_check.c:344
msgid "Check"
msgstr "Comprobar"

#: src/lib/elm_clock.c:303
msgid "clock increment button for am,pm"
msgstr "Botón de aumento del reloj para am,pm"

#: src/lib/elm_clock.c:311
msgid "clock decrement button for am,pm"
msgstr "Botón de decremento del reloj para am,pm"

#: src/lib/elm_clock.c:647
msgid "State: Editable"
msgstr "Estado: Editable"

#: src/lib/elm_clock.c:682
msgid "Clock"
msgstr "Reloj"

#: src/lib/elm_colorselector.c:731
msgid "Pick a color"
msgstr ""

#: src/lib/elm_colorselector.c:769
msgid "R:"
msgstr ""

#: src/lib/elm_colorselector.c:771
msgid "G:"
msgstr ""

#: src/lib/elm_colorselector.c:773
msgid "B:"
msgstr ""

#: src/lib/elm_colorselector.c:775
msgid "A:"
msgstr ""

# RR: Antiguamente - "Paleta de selección de color", pero creo que refiere a un selector como en las cadenas más abajo.
#: src/lib/elm_colorselector.c:1498
msgid "color selector palette item"
msgstr "Selector de color de paleta"

#: src/lib/elm_config.c:3287
msgid "default:LTR"
msgstr "Predeterminado: LTR"

#: src/lib/elm_dayselector.c:409
msgid "day selector item"
msgstr "Selector de día"

#: src/lib/elm_diskselector.c:700
msgid "diskselector item"
msgstr "Selector de disco"

#: src/lib/elm_entry.c:1575
msgid "Copy"
msgstr "Copiar"

#: src/lib/elm_entry.c:1580
msgid "Cut"
msgstr "Cortar"

#: src/lib/elm_entry.c:1584 src/lib/elm_entry.c:1607
msgid "Paste"
msgstr "Pegar"

#: src/lib/elm_entry.c:1600
msgid "Select"
msgstr "Seleccionar"

#: src/lib/elm_entry.c:3539
msgid "Entry"
msgstr "Entrada"

#: src/lib/elm_gengrid.c:1287
msgid "Gengrid Item"
msgstr "Cuadrícula genérica"

#: src/lib/elm_index.c:93
msgid "Index"
msgstr "Índice"

#: src/lib/elm_index.c:121
msgid "Index Item"
msgstr "Elemento del índice"

#: src/lib/elm_label.c:392
msgid "Label"
msgstr "Etiqueta"

#: src/lib/elm_panel.c:71
msgid "state: opened"
msgstr "estado: abierto"

#: src/lib/elm_panel.c:72
msgid "state: closed"
msgstr "estado: cerrado"

#: src/lib/elm_panel.c:112
msgid "A panel is open"
msgstr "Ya hay un panel abierto"

#: src/lib/elm_panel.c:114
msgid "Double tap to close panel menu"
msgstr "Doble click para cerrar el menú de panel"

#: src/lib/elm_panel.c:166
msgid "panel button"
msgstr "botón del panel"

#: src/lib/elm_progressbar.c:286
msgid "progressbar"
msgstr "barra de progreso"

#: src/lib/elm_radio.c:297
msgid "Radio"
msgstr "Radio"

#: src/lib/elm_segment_control.c:527 src/lib/elm_toolbar.c:2252
msgid "State: Selected"
msgstr "Estado: Seleccionado"

#: src/lib/elm_segment_control.c:529
msgid "State: Unselected"
msgstr "Estado: No seleccionado"

#: src/lib/elm_segment_control.c:543
msgid "Segment Control Item"
msgstr "Elemento del control del segmento"

#: src/lib/elm_slider.c:893
msgid "slider"
msgstr "deslizador"

#: src/lib/elm_spinner.c:932
msgid "incremented"
msgstr ""

#: src/lib/elm_spinner.c:938
msgid "decremented"
msgstr ""

#: src/lib/elm_spinner.c:971 src/lib/elm_spinner.c:1038
msgid "spinner"
msgstr "spinner"

#: src/lib/elm_spinner.c:980 src/lib/elm_spinner.c:1021
msgid "spinner increment button"
msgstr "botón de incremento del spinner"

#: src/lib/elm_spinner.c:983 src/lib/elm_spinner.c:1030
msgid "spinner decrement button"
msgstr "botón de decremento del spinner"

#: src/lib/elm_spinner.c:985
#, fuzzy
msgid "spinner text"
msgstr "spinner"

#: src/lib/elm_toolbar.c:1710 src/lib/elm_toolbar.c:2309
msgid "Selected"
msgstr "Seleccionado"

#: src/lib/elm_toolbar.c:2248
msgid "Separator"
msgstr "Separador"

#: src/lib/elm_toolbar.c:2254
msgid "Has menu"
msgstr "Tiene menú"

#: src/lib/elm_toolbar.c:2304
msgid "Unselected"
msgstr "Sin seleccionar"

#: src/lib/elm_toolbar.c:2321
msgid "Toolbar Item"
msgstr "Elemento de la barra de herramientas"

#~ msgid "Genlist Item"
#~ msgstr "Lista genérica"

#~ msgid "List Item"
#~ msgstr "Lista"
